#!/bin/bash

mkdir -p ./output/

for directorypath in ./PeeragogicalActionReviews/*/ ;
do
    # Only directory name.
    directory=${directorypath%/*}
    directory=${directory##*/}

    mkdir -p ./temp/
    mkdir -p ./temp/output/

    printf "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" > ./temp/jobfile_file_discovery_1.xml
    printf "<file-discovery-1-jobfile>\n" >> ./temp/jobfile_file_discovery_1.xml
    printf "  <input>\n" >> ./temp/jobfile_file_discovery_1.xml
    printf "    <directory path=\"../PeeragogicalActionReviews/%s/\"/>\n" $directory >> ./temp/jobfile_file_discovery_1.xml
    printf "  </input>\n" >> ./temp/jobfile_file_discovery_1.xml
    printf "  <settings>\n" >> ./temp/jobfile_file_discovery_1.xml
    printf "    <include-directories set=\"false\"/>\n" >> ./temp/jobfile_file_discovery_1.xml
    printf "    <include-files set=\"true\"/>\n" >> ./temp/jobfile_file_discovery_1.xml
    printf "  </settings>\n" >> ./temp/jobfile_file_discovery_1.xml
    printf "  <output>\n" >> ./temp/jobfile_file_discovery_1.xml
    printf "    <result-file path=\"./input.xml\"/>\n" >> ./temp/jobfile_file_discovery_1.xml
    printf "  </output>\n" >> ./temp/jobfile_file_discovery_1.xml
    printf "</file-discovery-1-jobfile>\n" >> ./temp/jobfile_file_discovery_1.xml

    java -cp ./digital_publishing_workflow_tools/file_discovery/file_discovery_1/ file_discovery_1 ./temp/jobfile_file_discovery_1.xml ./temp/resultinfo_file_discovery_1.xml
    java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./jobfile_xml_xslt_transformator_1.xml ./temp/resultinfo_xml_xslt_transformator_1_1.xml
    java -cp ./digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ xml_xslt_transformator_1 ./temp/jobfile_xml_xslt_transformator_1.xml ./temp/resultinfo_xml_xslt_transformator_1_2.xml

    mkdir -p ./output/$directory/
    cp ./temp/output/* ./output/$directory/

    rm -rf ./temp/

done
