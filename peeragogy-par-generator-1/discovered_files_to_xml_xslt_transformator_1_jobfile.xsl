<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" version="1.0" encoding="UTF-8" indent="yes"/>

  <xsl:template match="/">
    <xml-xslt-transformator-1-jobfile>
      <xsl:apply-templates select="./entry-list/entries/entry"/>
    </xml-xslt-transformator-1-jobfile>
  </xsl:template>

  <xsl:template match="/entry-list/entries/entry">
    <job input-file="{@path}" entities-resolver-config-file="../digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/entities/config_empty.xml" stylesheet-file="../PeeragogicalActionReviews/par_to_xhtml_1.xsl">
      <xsl:attribute name="output-file">
        <xsl:text>./output/</xsl:text>
        <xsl:number count="/entry-list/entries/entry" level="any"/>
        <xsl:text>.xhtml</xsl:text>
      </xsl:attribute>
    </job>
  </xsl:template>

  <xsl:template match="text()|node()|@*"/>

</xsl:stylesheet>
