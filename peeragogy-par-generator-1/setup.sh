#!/bin/sh
sudo apt-get install default-jre unzip wget

sleep 3s; wget https://github.com/Peeragogy/PeeragogicalActionReviews/archive/master.zip
unzip ./master.zip
mv ./PeeragogicalActionReviews-master/ ./PeeragogicalActionReviews/
rm ./master.zip
