#!/bin/sh
sudo apt-get install wget zip unzip make default-jdk

rm -r ./packages/peeragogy-par-generator-1/
mkdir -p ./packages/peeragogy-par-generator-1/
cp -r ./peeragogy-par-generator-1/ ./packages/

cd ./packages/
cd ./peeragogy-par-generator-1/

printf "Build date: $(date "+%Y-%m-%d").\n" > version.txt
currentDate=$(date "+%Y%m%d")

cd ..
cd ..


cd ./peeragogy-par-generator-1/
wget https://gitlab.com/publishing-systems/digital_publishing_workflow_tools/-/archive/master/digital_publishing_workflow_tools-master.zip
unzip digital_publishing_workflow_tools-master.zip
mv ./digital_publishing_workflow_tools-master/ ./digital_publishing_workflow_tools/
cd ./digital_publishing_workflow_tools/
make
cd workflows
cd setup
cd setup_1
java setup_1
cd ..
cd ..
cd ..
cd ..
cd ..


mkdir -p ./packages/peeragogy-par-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/
cp -r ./peeragogy-par-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/xml_xslt_transformator_1/ ./packages/peeragogy-par-generator-1/digital_publishing_workflow_tools/xml_xslt_transformator/

mkdir -p ./packages/peeragogy-par-generator-1/digital_publishing_workflow_tools/file_discovery/file_discovery_1/
cp -r ./peeragogy-par-generator-1/digital_publishing_workflow_tools/file_discovery/file_discovery_1/ ./packages/peeragogy-par-generator-1/digital_publishing_workflow_tools/file_discovery/



cd ./packages/

zip -r ./peeragogy-par-generator-1_$currentDate.zip peeragogy-par-generator-1
sha256sum ./peeragogy-par-generator-1_$currentDate.zip > ./peeragogy-par-generator-1_$currentDate.zip.sha256
rm -r ./peeragogy-par-generator-1/

cd ..
